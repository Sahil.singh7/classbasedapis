from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from .models import *

# Create your views here.

@api_view(['GET'])
def homePage(request):
    student_ob=Student.objects.all()
    serializer=StudentSerializer(student_ob,many='True')
    return Response({'status':200,'payload':serializer.data})


@api_view(['POST'])
def postData(request):
    serializer=StudentSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response({'status':200,'payload':serializer.data})

@api_view(['DELETE'])                                            
def deleteData(request,id):                                # urls.py to have proper path to delete, i.e, .../<id>/
    student_ob=Student.objects.get(id=id)
    student_ob.delete()
    return Response({'status':200,'message':'deleted'})
