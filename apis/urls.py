from django.urls import include,path
from .views import *

urlpatterns = [
    path('',homePage),
    path('posts',postData),
    path('deletes/<id>/',deleteData)  # important url to delete data
]

